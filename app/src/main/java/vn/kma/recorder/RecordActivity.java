package vn.kma.recorder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import vn.kma.recorder.databinding.ActivityRecordBinding;
import vn.kma.recorder.dialog.RenameActivity;
import vn.kma.recorder.model.Record;
import vn.kma.recorder.utils.DateUtils;
import vn.kma.recorder.utils.FileUtils;

public class RecordActivity extends AppCompatActivity {
    private static final String LOG_TAG = "RecordActivity";
    private static final int REQUEST_CODE = 102;
    private ActivityRecordBinding binding;

    MediaPlayer player;
    private String pathFile = null;

    Boolean playing = false;
    TimeCounterPlaying timeCounterPlaying;

    private long time = 0;
    private String recordNameTemp;
    private long timeCountTerTemp;
    private Record record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRecordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intent = getIntent();
        record = (Record) intent.getSerializableExtra("record-selected");
        Log.i(LOG_TAG, record.toString());

        pathFile = record.getPath();
        recordNameTemp = record.getFileName().replace(".mp3", "");
        timeCountTerTemp = record.getDuration();
        time = timeCountTerTemp;

        binding.recordName.setText(record.getFileName());
        binding.timeCounter.setText(DateUtils.timeLongDurationToDateString(record.getDuration()));

        // play button
        binding.btnPlay.setOnClickListener(v -> {
            timeCounterPlaying = new TimeCounterPlaying();
            playing = true;
            startPlaying();
            timeCounterPlaying.execute();

            // đổi nút hiển thị
            binding.btnPlay.setVisibility(View.GONE);
            binding.btnPause.setVisibility(View.VISIBLE);

        });

        // pause button
        binding.btnPause.setOnClickListener(v -> {
            playing = false;
            pausePlaying();

            // đổi nút hiển thị
            binding.btnPlay.setVisibility(View.VISIBLE);
            binding.btnPause.setVisibility(View.GONE);
        });

    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    private void startPlaying() {
        if (player != null) {
            if (time == 0) time = timeCountTerTemp;
            updateTimeCounter(time);
            player.start();
        } else {
            player = new MediaPlayer();
            try {
                player.setDataSource(pathFile);
                player.prepare();
                player.start();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() when play failed");
            }
        }

        Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
    }

    private void pausePlaying() {
        if (player != null) {
            player.pause();
            Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
        }
    }

    private void updateTimeCounter(Long timeLong) {
        String dateString = DateUtils.timeLongDurationToDateString(timeLong);

        // update ui
        binding.timeCounter.setText(dateString);
    }

    @SuppressLint("StaticFieldLeak")
    private class TimeCounterPlaying extends AsyncTask<Void, Long, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            System.out.println("Run Time Counter!");
            try {
                Thread.sleep(1000);
                while (playing && player.isPlaying() && time >= 1000) {

                    time -= 1000;
                    publishProgress(time);
                    Thread.sleep(1000);
                }
                if (time < 1000) {
                    time = timeCountTerTemp;
                    publishProgress(time);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            updateTimeCounter(values[0]);

            if (values[0] == timeCountTerTemp) {
                // đổi nút hiển thị
                binding.btnPlay.setVisibility(View.VISIBLE);
                binding.btnPause.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                shareFile();
                break;

            case R.id.rename:
                Intent intent = new Intent(RecordActivity.this, RenameActivity.class);
                intent.putExtra("record-rename", this.record);
                startActivityForResult(intent, REQUEST_CODE);
                break;

            case R.id.remove:
                if (FileUtils.removeFile(pathFile)) {
                    Toast.makeText(getApplicationContext(), "Remove file successful", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Remove file failure", Toast.LENGTH_SHORT).show();
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                record = (Record) data.getSerializableExtra(RenameActivity.EXTRA_DATA);
                binding.recordName.setText(record.getFileName());
                pathFile = record.getPath();
            }
        }
    }

    private void shareFile() {
        File file = new File(record.getPath());

        if (!file.exists()) {
            Toast.makeText(getApplicationContext(), "File don't exists", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intentShare = new Intent(Intent.ACTION_SEND);
        intentShare.setType("audio/mpeg");
        intentShare.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

        startActivity(Intent.createChooser(intentShare, "Share the file ..."));
    }
}
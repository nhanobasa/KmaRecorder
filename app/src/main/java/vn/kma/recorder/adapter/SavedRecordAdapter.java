package vn.kma.recorder.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import vn.kma.recorder.R;
import vn.kma.recorder.RecordActivity;
import vn.kma.recorder.fragment.SavedRecordingFragment;
import vn.kma.recorder.model.Record;
import vn.kma.recorder.utils.DateUtils;
import vn.kma.recorder.utils.OnIntentReceived;

public class SavedRecordAdapter extends RecyclerView.Adapter<SavedRecordAdapter.SavedRecordViewHolder> implements OnIntentReceived {
    private static final String LOG_TAG = "SavedRecordAdapter";
    public static final int REQUEST_CODE = 101;
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Context context;
    private List<Record> data;

    public SavedRecordAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public SavedRecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.i(LOG_TAG, "Creating view holder");
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.record_item, parent, false);
        return new SavedRecordViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedRecordAdapter.SavedRecordViewHolder holder, int position) {
        Record record = data.get(position);
        holder.txtRecordName.setText(record.getFileName());
        holder.txtRecordDuration.setText(DateUtils.timeLongDurationToDateString(record.getDuration()));
        holder.txtRecordLastModified.setText(sdf.format(new Date(record.getLastModified())));

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, RecordActivity.class);
            intent.putExtra("record-selected", record);
            ((Activity) context).startActivityForResult(intent, REQUEST_CODE);
        });
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }
        return data.size();
    }

    public void setData(List<Record> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<Record> getData() {
        return this.data;
    }

    public static class SavedRecordViewHolder extends RecyclerView.ViewHolder {
        TextView txtRecordName;
        TextView txtRecordDuration;
        TextView txtRecordLastModified;
        CardView cardView;

        public SavedRecordViewHolder(@NonNull View itemView) {
            super(itemView);
            txtRecordName = itemView.findViewById(R.id.txt_record_name);
            txtRecordDuration = itemView.findViewById(R.id.txt_record_duration);
            txtRecordLastModified = itemView.findViewById(R.id.txt_last_modified);
            cardView = itemView.findViewById(R.id.card_view_record);
        }
    }

    @Override
    public void onIntent(Intent i, int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            Log.i(LOG_TAG, "HELLO");
        }
    }
}

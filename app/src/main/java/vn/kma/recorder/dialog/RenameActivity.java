package vn.kma.recorder.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import vn.kma.recorder.databinding.ActivityRenameBinding;
import vn.kma.recorder.model.Record;
import vn.kma.recorder.utils.FileUtils;

public class RenameActivity extends AppCompatActivity {
    public static final String EXTRA_DATA = "record-rename-ok";
    private ActivityRenameBinding binding;
    private Record record;
    String recordNameTemp = "";
    String oldPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = ActivityRenameBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Intent intent = getIntent();
        record = (Record) intent.getSerializableExtra("record-rename");

        oldPath = record.getPath();
        String recordName = record.getFileName();
        recordNameTemp = recordName.replace(".mp3", "");
        binding.txtEditRecordName.setText(recordNameTemp);
        binding.txtEditRecordName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.txtEditRecordName.setSelectAllOnFocus(true);
                }
            }
        });


        // cancel button
        binding.btnCancel.setOnClickListener(v -> {
            finish();
        });

        // save button
        binding.btnSave.setOnClickListener(v -> {
            String newName = binding.txtEditRecordName.getText().toString();
            if (newName.equals(recordNameTemp)) {
                binding.txtEditRecordName.setError("Tên bản ghi đã tồn tại");
            } else {
                String newPath = oldPath.replace(recordNameTemp, newName);
                if (FileUtils.renameFile(oldPath, newPath)) {
                    Toast.makeText(getApplicationContext(), "Rename file successful", Toast.LENGTH_SHORT).show();
                    record.setFileName(newName);
                    record.setPath(newPath);
                    setResult(RESULT_OK, new Intent().putExtra(EXTRA_DATA, record));
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Rename file failure", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
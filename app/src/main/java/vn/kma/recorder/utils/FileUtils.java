package vn.kma.recorder.utils;

import android.media.MediaMetadataRetriever;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import vn.kma.recorder.model.Record;

public class FileUtils {
    public static List<String> getAllFileAndDirInPath(String path) {
        List<String> list = new ArrayList<>();

        File file = new File(path);
        list = Arrays.asList(Objects.requireNonNull(file.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return  name.endsWith(".mp3");
            }
        })));

        return list;
    }

    public static List<Record> getAllRecordFile(String appDirectoryPath) {
        List<String> fileNameList = getAllFileAndDirInPath(appDirectoryPath);
        List<Record> recordList = new ArrayList<>();

        for (String fileName : fileNameList) {
            String pathFile = appDirectoryPath + File.separator + fileName;
            Long audioDuration = getAudioDuration(pathFile);

            Record record = new Record(fileName, audioDuration, getLastModified(pathFile), pathFile);

            recordList.add(record);
        }

        return recordList;
    }

    public static Long getLastModified(String path) {
        File file = new File(path);
        return file.lastModified();
    }

    public static Long getAudioDuration(String path) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long millSecond = Long.parseLong(duration);

        return millSecond;
    }

    public static boolean removeFile(String path) {
        File file = new File(path);
        boolean result = false;
        if (file.isFile() || file.exists()) {
            result = file.delete();
        }
        return result;
    }

    public static boolean renameFile(String oldPath, String newPath) {
        File file = new File(oldPath);
        boolean result = false;
        try {
            if (file.isFile() || file.exists()) {
                File newFile = new File(newPath);

                // đổi tên file
                result = file.renameTo(newFile);
                if (result) {
                    // đổi last modified
                    result = newFile.setLastModified(System.currentTimeMillis());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

package vn.kma.recorder.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static SimpleDateFormat phutGiay = new SimpleDateFormat("mm:ss");

    public static String timeLongDurationToDateString(Long timeLong) {
        Date date = new Date(timeLong);
        SimpleDateFormat phutGiay = new SimpleDateFormat("mm:ss");
        String dateString = phutGiay.format(date);

        return dateString;
    }
}

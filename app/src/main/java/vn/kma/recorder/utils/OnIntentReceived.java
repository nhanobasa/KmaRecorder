package vn.kma.recorder.utils;

import android.content.Intent;

public interface OnIntentReceived {
    void onIntent(Intent i, int resultCode);
}

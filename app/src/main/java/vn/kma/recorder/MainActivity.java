package vn.kma.recorder;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.io.File;

import vn.kma.recorder.databinding.ActivityMainBinding;
import vn.kma.recorder.fragment.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";
    ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Khởi tạo view
        initView();
        initAppDirectory();

    }

    private void initView() {
        ViewPagerAdapter viewPagerAdapter =
                new ViewPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

    }

    private void initAppDirectory() {
        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "KmaRecorder");
        if (!directory.exists() || !directory.isDirectory()) {
            directory.mkdirs();
            Log.d(LOG_TAG, "Init App Directory successful!");
        }
    }
}
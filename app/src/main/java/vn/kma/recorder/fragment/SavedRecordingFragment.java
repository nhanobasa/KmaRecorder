package vn.kma.recorder.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vn.kma.recorder.R;
import vn.kma.recorder.adapter.SavedRecordAdapter;
import vn.kma.recorder.databinding.FragmentRecordBinding;
import vn.kma.recorder.databinding.FragmentSavedRecordingBinding;
import vn.kma.recorder.model.Record;
import vn.kma.recorder.utils.FileUtils;
import vn.kma.recorder.utils.OnIntentReceived;

public class SavedRecordingFragment extends Fragment {

    private static final String APP_DIRECTORY_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "KmaRecorder";
    private static final String LOG_TAG = "SavedRecordingFragment";

    private FragmentSavedRecordingBinding binding;
    private SavedRecordAdapter adapter;
    private OnIntentReceived mIntentListener;


    private List<Record> recordList;

    public SavedRecordingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new SavedRecordAdapter(getContext());
        mIntentListener = adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_saved_recording, container, false);
        binding = FragmentSavedRecordingBinding.bind(v);

        // Xử lý recycler view cho phần gợi ý top job
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerviewSavedRecord.setLayoutManager(llm);
        binding.recyclerviewSavedRecord.setAdapter(adapter);

        return binding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("Resume");
        setData();
    }

    private void setData() {
        // get all file in app dir
        recordList = new ArrayList<>();
        recordList = FileUtils.getAllRecordFile(APP_DIRECTORY_PATH);
        adapter.setData(recordList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SavedRecordAdapter.REQUEST_CODE) {
            if (mIntentListener != null) {
                mIntentListener.onIntent(data, resultCode);
            }
        }
    }

}
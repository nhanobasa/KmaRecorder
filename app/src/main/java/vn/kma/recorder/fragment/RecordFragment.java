package vn.kma.recorder.fragment;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import org.apache.commons.lang3.time.StopWatch;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import vn.kma.recorder.R;
import vn.kma.recorder.databinding.FragmentRecordBinding;
import vn.kma.recorder.utils.DateUtils;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class RecordFragment extends Fragment {
    private MediaRecorder recorder;
    private String outputFile = null;
    MediaPlayer player;
    FragmentRecordBinding binding;
    StopWatch stopWatch;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss");
    TimeCounter timeCounter;
    TimeCounterPlaying timeCounterPlaying;
    Boolean runTimeCounter = false;
    Boolean playing = false;
    private static final String LOG_TAG = "AudioRecord";
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private long time = 0;
    private String recordNameTemp;
    private long timeCountTerTemp;


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, REQUEST_AUDIO_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        // this method is called when user will
        // grant the permission for audio recording.
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }


    public RecordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_record, container, false);
        binding = FragmentRecordBinding.bind(view);
        Date date = new Date(System.currentTimeMillis());
        binding.recordName.setText(sdf.format(date));
        recordNameTemp = binding.recordName.getText().toString();
        // main process
        process();

        return binding.getRoot();
    }

    private void process() {
        // khi người dùng bấm nút record
        binding.btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player != null && !player.isPlaying()) playing = false;
                if (checkPermission() && !playing) {
                    player = null;

                    binding.btnPlay.setVisibility(View.GONE);
                    binding.btnPause.setVisibility(View.GONE);

                    // Nếu recordName đang true thì sẽ đổi tên mới theo time và setEnabled(false)
                    if (binding.recordName.isEnabled()) {
                        if (recordNameTemp.equals(binding.recordName.getText().toString())) {
                            Date date = new Date(System.currentTimeMillis());
                            binding.recordName.setText(sdf.format(date));

                        }

                        binding.recordName.setEnabled(false);
                    }

                    // tạo đường dẫn để lưu file ghi âm
                    outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KmaRecorder/" + binding.recordName.getText().toString().trim() + ".mp3";
                    Log.d(LOG_TAG, "OUTPUT_PATH: " + outputFile);

                    try {
                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                        recorder.setOutputFile(outputFile);
                        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

                        recorder.prepare();
                        recorder.start();

                        // KHI BẮT ĐẦU GHI
                        // Chạy luồng để thay đổi time counter
                        timeCounter = new TimeCounter();
                        runTimeCounter = true;
                        timeCounter.execute();

                        // Chuyển sang nút pause
                        binding.btnRecord.setVisibility(View.GONE);
                        binding.btnStopRecord.setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    requestPermission();
                }

            }

        });

        // Khi người dùng bấm nút stop record
        binding.btnStopRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hoàn thành bản ghi âm
                try {
                    stopRecording();

                    binding.btnPlay.setVisibility(View.VISIBLE);
                    binding.btnPause.setVisibility(View.VISIBLE);

                    // Tắt time counter thread
                    runTimeCounter = false;

                    // reset lại time counter text
//                    binding.timeCounter.setText("00:00");

                    // bật lại nút record và tắt nút pause
                    binding.btnRecord.setVisibility(View.VISIBLE);
                    binding.btnStopRecord.setVisibility(View.GONE);
                    binding.recordName.setEnabled(true);
                    recordNameTemp = binding.recordName.getText().toString();

                    Toast.makeText(getContext(), "Audio recorded successfully", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        // play button
        binding.btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeCounterPlaying = new TimeCounterPlaying();
                playing = true;
                startPlaying();
                timeCounterPlaying.execute();
            }
        });

        // pause button
        binding.btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playing = false;
                pausePlaying();
            }
        });

    }

    private class TimeCounter extends AsyncTask<Void, Long, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            System.out.println("Run Time Counter!");
            stopWatch = StopWatch.createStarted();
            while (runTimeCounter) {
                try {
                    time = stopWatch.getTime();
                    timeCountTerTemp = time;
                    publishProgress(time);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            updateTimeCounter(values[0]);
        }
    }

    private class TimeCounterPlaying extends AsyncTask<Void, Long, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            System.out.println("Run Time Counter!");
            try {
                Thread.sleep(1000);
                while (playing && player.isPlaying() && time >= 1000) {

                    time -= 1000;
                    publishProgress(time);
                    Thread.sleep(1000);
                }
                if (time < 1000) time = 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            updateTimeCounter(values[0]);
        }
    }

    private void pausePlaying() {
        if (player != null) {
            player.pause();
            Toast.makeText(getContext(), "Playing audio", Toast.LENGTH_LONG).show();
        }
    }

    private void startPlaying() {
        if (player != null) {
            if (time == 0) time = timeCountTerTemp;
            updateTimeCounter(time);
            player.start();
        } else {
            player = new MediaPlayer();
            try {
                player.setDataSource(outputFile);
                player.prepare();
                player.start();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() when play failed");
            }
        }

        Toast.makeText(getContext(), "Playing audio", Toast.LENGTH_LONG).show();
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }

        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void updateTimeCounter(Long timeLong) {
        String dateString = DateUtils.timeLongDurationToDateString(timeLong);

        // update ui
        binding.timeCounter.setText(dateString);
    }
}
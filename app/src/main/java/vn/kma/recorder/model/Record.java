package vn.kma.recorder.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Record implements Serializable {
    private String fileName;
    private Long duration;
    private Long lastModified;
    private String path;


    public Record() {
        super();
    }

    public Record(String fileName, Long duration, Long lastModified) {
        this.fileName = fileName;
        this.duration = duration;
        this.lastModified = lastModified;
    }

    public Record(String fileName, Long duration, Long lastModified, String path) {
        this.fileName = fileName;
        this.duration = duration;
        this.lastModified = lastModified;
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Record{" +
                "fileName='" + fileName + '\'' +
                ", duration=" + duration +
                ", lastModified=" + lastModified +
                ", path='" + path + '\'' +
                '}';
    }
}
